# SystemTap tracepoint operational tests.
Tracepoint operational tests system tracing and probing with SystemTap tool.

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
