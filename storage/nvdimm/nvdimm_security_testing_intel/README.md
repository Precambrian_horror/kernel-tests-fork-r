# storage/nvdimm/nvdimm_security_testing_intel

Storage: intel aep nvdimm security testing

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
