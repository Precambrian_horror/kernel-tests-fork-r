summary: Run stress-ng tests
description: |
    Run stress-ng tests
      git://kernel.ubuntu.com/cking/stress-ng.git

    Note: If using classes, the timeout is per stressor, not the
    whole class.  Run 'stress-ng --class interrupt?' to see all the
    stressors in the interrupt class.  Multiply the number of
    stressors by the timeout to get the expected run time.

    This task uses a list of stressors (see the *.stressors files)
    with a 5 second timeout (by default) for each.  There are 184
    stressors, so 184 * 5 = expected runtime of 920 seconds.

    TASK PARAMETERS
    ---------------
    GIT_URL = URL to stress-ng git repo
              default: git://kernel.ubuntu.com/cking/stress-ng.git

    GIT_BRANCH = version of stress-ng to check out
                 default: see runtest.sh
contact: Jeff Bastian <jbastian@redhat.com>
test: bash ./runtest.sh
framework: beakerlib
require:
  - wget
  - git
  - time
  - patch
  - bzip2
  - autoconf
  - glib2-devel
  - make
  - gettext
  - automake
  - gcc
  - libtool
  - bison
  - flex
  - libcap-devel
  - zlib-devel
  - coreutils
  - beakerlib
recommend:
  - dmidecode
  - rpmdevtools
  - libaio-devel
  - libattr-devel
  - libbsd-devel
  - libgcrypt-devel
  - libsctp-devel
  - keyutils-libs
  - beakerlib-redhat
extra-summary: stress/stress-ng
extra-task: stress/stress-ng
link:
  - implements: https://polarion.engineering.redhat.com/polarion/#/project/RHELKERNEL/workitem?id=KERNEL-291767
id: 78648bc2-5de1-4390-b0b8-fbe37d31b064
